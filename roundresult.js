var fResult = 0, fExp = 0, fMasteryBuff = 1, fDropBuff = 1, fDrop = 1;

// Courtesy of http://stackoverflow.com/a/14570614

var observeDOM = (function(){
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
        eventListenerSupported = window.addEventListener;

    return function(obj, callback){
        if( MutationObserver ){
            // define a new observer
            var obs = new MutationObserver(function(mutations, observer){
                if( mutations[0].addedNodes.length || mutations[0].removedNodes.length )
                    callback();
            });
            // have the observer observe foo for changes in children
            obs.observe( obj, { childList:true, subtree:true });
        }
        else if( eventListenerSupported ){
            obj.addEventListener('DOMNodeInserted', callback, false);
            obj.addEventListener('DOMNodeRemoved', callback, false);
        }
    }
})();

// Observe a specific DOM element:
observeDOM(document.getElementById('skillResults') ,function(){

    // Gets the experience amount as int and adds it to previous.
    var fExptmp = document.getElementsByClassName("roundResult")[0].getElementsByClassName("statValue")[0];
    var fDroptmp = document.getElementsByClassName("roundResult")[0].children[1].nextSibling.nodeValue;
    var fMasteryBufftmp = document.getElementsByClassName("roundResult")[0].getElementsByClassName("masteryValue")[0];
    var fDropBufftmp = document.getElementsByClassName("roundResult")[0].getElementsByClassName("buffValue")[0];

    if ( fExptmp != null ) {
            console.log("fExptmp");
            fExp = fExp + parseInt(fExptmp.innerText.match(/\d+/)[0]);
            console.log('Success! fExp updated! ' + fExp);
    }


    if ( fDroptmp != null ) {
            console.log("fDroptmp");
            fDrop = fDrop + parseInt(fDroptmp.match(/\d+/)[0]);
            console.log('Success! fDrop updated! ' + fDrop);
    }
    
    // Gets the mastery drop value
    
    if ( fMasteryBufftmp != null ) {
            console.log("fBufftmp");
            fMasteryBuff = fMasteryBuff + parseInt(fMasteryBufftmp.innerText.match(/\d+/)[0]);
            console.log('Success! fMasteryBuff updated! ' + fMasteryBuff);
    }
    
    // gets the buff drop value
    if ( fDropBufftmp != null ) {
            console.log("fDropBufftmp")
            fDropBuff = fDropBuff + parseInt(fDropBufftmp.innerText.match(/\d+/)[0]);
            console.log('Success! fDropBuff updated! ' + fDropBuff);
    }
});


// possible checks to make sure im getting all the damn results
//var f_fi = 0;
//do {
//    var fDropBufftmp = document.getElementsByClassName("roundResult")[f_fi].getElementsByClassName("buffValue")[f_fi];
//        if ( fDropBufftmp != null ) {
//            fDropBuff = fDropBuff + parseInt(fDropBufftmp.innerText.match(/\d+/)[0]);
//            console.log('Success! fMasteryBuff updated! ' + fDropBuff);
//        f_fi += 1
//        }
//} while (document.getElementsByClassName("skillResults")[0].getElementsByClassName("roundResult")[f_fi].getElementsByClassName("statValue")[0] != null);
    

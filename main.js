//What Data Do I Need?
//
//    -Drops/Creation
//        * Amount
//            -Is it empty (?)
//        * Type
//        * Mastery Bonus (?)
//        * Drop/Creation Rate Bonus (?)
//        * Mystery Gold Perk (?)
//        * Time
//            -Start Time
//            -End Time
//            -Total Time

//vars
var tDropTypeID, tDropAmount, tExpAmount, tGoldPerk, tResults, tMasteryAmount
var ffDropAmount, ffExpAmount, ffGoldPerk, ffResults, ffMasteryAmount, i
var ffDropTypeID = {
    failures : 0,
    Drop_List : {}
}

//get just text
jQuery.fn.justtext = function() {
    //courtesy of
    //http://viralpatel.net/blogs/jquery-get-text-element-without-child-element/
	return $(this)	.clone()
			.children()
			.remove()
			.end()
			.text();

};

//Get Skill Results
$("#skillResults > div:nth-child(1) > span.viewMat");

function parseDefaultResults() {

    if ($("#skillResults > div:nth-child(10)").justtext() !== " ") {
        if ($("#skillResults > div:nth-child(1)").length) {
            tDropAmount = parseInt($("#skillResults > div:nth-child(1)").justtext().match(/\d+/)[0]);
            tDropTypeID = $("#skillResults > div:nth-child(1) > span.viewMat").attr('id');
        }
        if ($("#skillResults > div:nth-child(1) > span.statValue").length) {
            tExpAmount = parseInt($("#skillResults > div:nth-child(1) > span.statValue").justtext().match(/\d+/)[0]);
        }
        if ($("#skillResults > div:nth-child(34) > span.masteryValue").length) {
            tMasteryAmount = parseInt($("#skillResults > div:nth-child(34) > span.masteryValue").justtext().match(/\d+/)[0]);
        }
        if ($("#skillResults > div:nth-child(1) span.perkValue").length) {
            tGoldPerk = parseInt($("#skillResults > div:nth-child(1) span.perkValue").justtext().match(/\d+/)[0]);
        }
    } else {
        ffDropTypeID.failures += 1;
    }
    
    return tDropTypeID, tDropAmount, tExpAmount, tMasteryAmount, tGoldPerk;
};

function printResults() {
    console.log(tDropTypeID, ffDropTypeID.failures, tDropAmount, tExpAmount, tMasteryAmount, tGoldPerk)
}

function getResults() {
    var tResults = $("#skillResults > div:nth-child(1)");
    if (tResults) {
        parseDefaultResults();
        ffDropTypeID.Drop_List[i] = tDropTypeID;
        i++;
        ffDropAmount += tDropAmount;
        ffExpAmount += tExpAmount;
        ffGoldPerk += tGoldPerk;
        ffMasteryAmount += tMasteryAmount;
    }
};

//On #skillResults update
$('#skillResults').bind("DOMSubtreeModified",function(){
  console.log("updated");
});



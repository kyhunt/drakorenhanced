// ==UserScript==
// @name         Max-Button-Script
// @version      1.0
// @description  Enables a "MAX" button on any pattern-related skill
// @author       Bl00D4NGEL
// @match        http://www.drakor.com/
// @link         http://pastebin.com/uvv2Cw9n
// ==/UserScript==
 
setInterval(function(){
if(document.getElementsByClassName("patternBody").length !== 0){
var btn = document.createElement("BUTTON");
var btnText = document.createTextNode("MAX");
btn.appendChild(btnText);btn.addEventListener("click", function(){document.getElementsByClassName("thinInputsm")[document.getElementsByClassName("thinInputsm").length-1].value = document.getElementsByClassName("thinInputsm")[document.getElementsByClassName("thinInputsm").length-1].length;
});
for(var i=0;i <document.getElementsByClassName("patternBody").length;i++){
if(document.getElementsByClassName("patternBody")[i].lastChild.innerHTML !== "MAX"){document.getElementsByClassName("patternBody")[i].appendChild(btn);
}
}
}
},1000);

var i = 3, person = {}, people = [];

while($("#statResult").children[i]) {
    var id = parseInt($("#statResult > div:nth-child(" + i + ") > a").href.match(/\d+/));
    var name = $("#statResult > div:nth-child(" + i + ") > a > span").innerText;
    person[name] = id;
    i += 1;
}
